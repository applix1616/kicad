EESchema Schematic File Version 2
LIBS:applix1616-sch
LIBS:power
LIBS:device
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:onsemi
LIBS:transistors
LIBS:74xgxx
LIBS:ttl_ieee
LIBS:parallax_propeller
LIBS:Lattice
LIBS:applix1616-s00-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date "26 nov 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74LS166 U29
U 1 1 5A192C9C
P 8600 1900
F 0 "U29" H 8600 2150 60  0000 C CNN
F 1 "74LS166" H 8600 1950 60  0000 C CNN
F 2 "~" H 8600 1900 60  0000 C CNN
F 3 "~" H 8600 1900 60  0000 C CNN
	1    8600 1900
	1    0    0    -1  
$EndComp
$Comp
L 74LS166 U31
U 1 1 5A192CAB
P 8600 4700
F 0 "U31" H 8600 4950 60  0000 C CNN
F 1 "74LS166" H 8600 4750 60  0000 C CNN
F 2 "~" H 8600 4700 60  0000 C CNN
F 3 "~" H 8600 4700 60  0000 C CNN
	1    8600 4700
	1    0    0    -1  
$EndComp
Text HLabel 10500 2050 2    60   Output ~ 0
VD0
Text HLabel 10500 4850 2    60   Output ~ 0
VD1
$Sheet
S 2700 1100 1100 750 
U 5A1A5200
F0 "DRAM-BANK0" 50
F1 "applix1616-s03.sch" 50
F2 "D[0..7]" I L 2700 1200 60 
F3 "W" I L 2700 1400 60 
F4 "RAS" I L 2700 1500 60 
F5 "CAS" I L 2700 1600 60 
F6 "RA[0..8]" I L 2700 1700 60 
F7 "VID[0..7]" O R 3800 1200 60 
$EndSheet
$Sheet
S 2700 3750 1100 750 
U 5A1A5204
F0 "DRAM-BANK1" 50
F1 "applix1616-S04.sch" 50
F2 "D[8..15]" I L 2700 3850 60 
F3 "W" I L 2700 4050 60 
F4 "RAS" I L 2700 4150 60 
F5 "CAS" I L 2700 4250 60 
F6 "RA[0..8]" I L 2700 4350 60 
F7 "VID[8..15]" O R 3800 3850 60 
$EndSheet
$Comp
L VCC #PWR?
U 1 1 5A1A6FF7
P 7750 1100
F 0 "#PWR?" H 7750 1200 30  0001 C CNN
F 1 "VCC" H 7750 1200 30  0000 C CNN
F 2 "" H 7750 1100 60  0000 C CNN
F 3 "" H 7750 1100 60  0000 C CNN
	1    7750 1100
	1    0    0    -1  
$EndComp
Entry Wire Line
	7200 1450 7300 1550
Entry Wire Line
	7200 1350 7300 1450
Entry Wire Line
	7200 1250 7300 1350
Entry Wire Line
	7200 1550 7300 1650
Entry Wire Line
	7200 1650 7300 1750
Entry Wire Line
	7200 1750 7300 1850
Entry Wire Line
	7200 1850 7300 1950
Entry Wire Line
	7200 1950 7300 2050
Entry Wire Line
	7200 4150 7300 4250
Entry Wire Line
	7200 4050 7300 4150
Entry Wire Line
	7200 4250 7300 4350
Entry Wire Line
	7200 4350 7300 4450
Entry Wire Line
	7200 4550 7300 4650
Entry Wire Line
	7200 4650 7300 4750
Entry Wire Line
	7200 4750 7300 4850
Entry Wire Line
	7200 4450 7300 4550
Text Label 7350 1350 0    60   ~ 0
VID0
Text Label 7350 1450 0    60   ~ 0
VID2
Text Label 7350 1550 0    60   ~ 0
VID4
Text Label 7350 1650 0    60   ~ 0
VID6
Text Label 7350 1750 0    60   ~ 0
VID8
Text Label 7350 1850 0    60   ~ 0
VID10
Text Label 7350 1950 0    60   ~ 0
VID12
Text Label 7350 2050 0    60   ~ 0
VID14
Text Label 7350 4150 0    60   ~ 0
VID1
Text Label 7350 4250 0    60   ~ 0
VID3
Text Label 7350 4350 0    60   ~ 0
VID5
Text Label 7350 4450 0    60   ~ 0
VID7
Text Label 7350 4550 0    60   ~ 0
VID9
Text Label 7350 4650 0    60   ~ 0
VID11
Text Label 7350 4750 0    60   ~ 0
VID13
Text Label 7350 4850 0    60   ~ 0
VID15
$Comp
L GND #PWR?
U 1 1 5A1AE331
P 7700 5850
F 0 "#PWR?" H 7700 5850 30  0001 C CNN
F 1 "GND" H 7700 5780 30  0001 C CNN
F 2 "" H 7700 5850 60  0000 C CNN
F 3 "" H 7700 5850 60  0000 C CNN
	1    7700 5850
	1    0    0    -1  
$EndComp
Text HLabel 5200 2300 0    60   Input ~ 0
VCLK
Text HLabel 5200 2200 0    60   Input ~ 0
LPULSE
$Comp
L 74LS244 U30
U 1 1 5A1AEF8A
P 3250 2650
F 0 "U30" H 3300 2450 60  0000 C CNN
F 1 "74LS244" H 3350 2250 60  0000 C CNN
F 2 "~" H 3250 2650 60  0000 C CNN
F 3 "~" H 3250 2650 60  0000 C CNN
	1    3250 2650
	-1   0    0    -1  
$EndComp
$Comp
L 74LS244 U28
U 1 1 5A1AEF99
P 3250 5500
F 0 "U28" H 3300 5300 60  0000 C CNN
F 1 "74LS244" H 3350 5100 60  0000 C CNN
F 2 "~" H 3250 5500 60  0000 C CNN
F 3 "~" H 3250 5500 60  0000 C CNN
	1    3250 5500
	-1   0    0    -1  
$EndComp
Entry Wire Line
	4600 2150 4700 2050
Entry Wire Line
	4600 2250 4700 2150
Entry Wire Line
	4600 2350 4700 2250
Entry Wire Line
	4600 2450 4700 2350
Entry Wire Line
	4600 2550 4700 2450
Entry Wire Line
	4600 2650 4700 2550
Entry Wire Line
	4600 2750 4700 2650
Entry Wire Line
	4600 2850 4700 2750
Entry Wire Line
	4600 5000 4700 4900
Entry Wire Line
	4600 5100 4700 5000
Entry Wire Line
	4600 5200 4700 5100
Entry Wire Line
	4600 5300 4700 5200
Entry Wire Line
	4600 5400 4700 5300
Entry Wire Line
	4600 5500 4700 5400
Entry Wire Line
	4600 5600 4700 5500
Entry Wire Line
	4600 5700 4700 5600
Text Label 4350 2150 0    60   ~ 0
VID0
Text Label 4350 2850 0    60   ~ 0
VID1
Text Label 4350 2250 0    60   ~ 0
VID2
Text Label 4350 2750 0    60   ~ 0
VID3
Text Label 4350 2350 0    60   ~ 0
VID4
Text Label 4350 2650 0    60   ~ 0
VID5
Text Label 4350 2450 0    60   ~ 0
VID6
Text Label 4350 2550 0    60   ~ 0
VID7
Text Label 4300 5000 0    60   ~ 0
VID15
Text Label 4300 5100 0    60   ~ 0
VID13
Text Label 4300 5200 0    60   ~ 0
VID11
Text Label 4300 5300 0    60   ~ 0
VID8
Text Label 4300 5400 0    60   ~ 0
VID9
Text Label 4300 5500 0    60   ~ 0
VID10
Text Label 4300 5600 0    60   ~ 0
VID12
Text Label 4300 5700 0    60   ~ 0
VID14
Text HLabel 850  3300 0    60   Input ~ 0
DBRE
Wire Wire Line
	9300 2050 10500 2050
Wire Wire Line
	9300 4850 10500 4850
Wire Wire Line
	7900 1250 7750 1250
Wire Wire Line
	7750 1100 7750 5350
Wire Bus Line
	7200 1200 7200 4750
Wire Bus Line
	3800 1200 7200 1200
Wire Wire Line
	7900 1350 7300 1350
Wire Wire Line
	7900 1450 7300 1450
Wire Wire Line
	7900 1550 7300 1550
Wire Wire Line
	7900 1650 7300 1650
Wire Wire Line
	7300 1750 7900 1750
Wire Wire Line
	7900 1850 7300 1850
Wire Wire Line
	7300 1950 7900 1950
Wire Wire Line
	7900 2050 7300 2050
Wire Bus Line
	7200 3850 3800 3850
Wire Wire Line
	7750 4050 7900 4050
Connection ~ 7750 1250
Wire Wire Line
	7900 4150 7300 4150
Wire Wire Line
	7300 4250 7900 4250
Wire Wire Line
	7300 4350 7900 4350
Wire Wire Line
	7900 4450 7300 4450
Wire Wire Line
	7300 4550 7900 4550
Wire Wire Line
	7900 4650 7300 4650
Wire Wire Line
	7300 4750 7900 4750
Wire Wire Line
	7900 4850 7300 4850
Wire Wire Line
	7900 2550 7750 2550
Connection ~ 7750 2550
Wire Wire Line
	7750 5350 7900 5350
Connection ~ 7750 4050
Wire Wire Line
	7900 2400 7700 2400
Wire Wire Line
	7700 2400 7700 5850
Wire Wire Line
	7900 5200 7700 5200
Connection ~ 7700 5200
Wire Wire Line
	5200 2300 7900 2300
Wire Wire Line
	7100 2300 7100 5100
Wire Wire Line
	7100 5100 7900 5100
Connection ~ 7100 2300
Wire Wire Line
	5200 2200 7900 2200
Wire Wire Line
	7900 5000 7050 5000
Wire Wire Line
	7050 5000 7050 2200
Connection ~ 7050 2200
Wire Bus Line
	4700 1200 4700 2750
Wire Bus Line
	4700 3850 4700 5600
Wire Wire Line
	3950 5000 4600 5000
Wire Wire Line
	3950 5100 4600 5100
Wire Wire Line
	4600 5200 3950 5200
Wire Wire Line
	3950 5300 4600 5300
Wire Wire Line
	4600 5400 3950 5400
Wire Wire Line
	3950 5500 4600 5500
Wire Wire Line
	4600 5600 3950 5600
Wire Wire Line
	3950 5700 4600 5700
Wire Wire Line
	3950 2850 4600 2850
Wire Wire Line
	4600 2750 3950 2750
Wire Wire Line
	3950 2650 4600 2650
Wire Wire Line
	3950 2550 4600 2550
Wire Wire Line
	3950 2450 4600 2450
Wire Wire Line
	3950 2350 4600 2350
Wire Wire Line
	3950 2250 4600 2250
Wire Wire Line
	3950 2150 4600 2150
Wire Wire Line
	4200 6000 3950 6000
Wire Wire Line
	4200 3050 4200 6000
Wire Wire Line
	4200 3050 3950 3050
Wire Wire Line
	3950 3150 4200 3150
Connection ~ 4200 3150
Wire Wire Line
	3950 5900 4200 5900
Connection ~ 4200 5900
Wire Wire Line
	850  3300 4200 3300
Connection ~ 4200 3300
Wire Bus Line
	1800 900  1800 5600
Entry Wire Line
	1800 2850 1900 2750
Entry Wire Line
	1800 2950 1900 2850
Entry Wire Line
	1800 2850 1900 2750
Entry Wire Line
	1800 2650 1900 2550
Entry Wire Line
	1800 2750 1900 2650
Entry Wire Line
	1800 2550 1900 2450
Entry Wire Line
	1800 2450 1900 2350
Entry Wire Line
	1800 2350 1900 2250
Entry Wire Line
	1800 2250 1900 2150
Wire Wire Line
	2550 2150 1900 2150
Wire Wire Line
	1900 2250 2550 2250
Wire Wire Line
	2550 2350 1900 2350
Wire Wire Line
	1900 2450 2550 2450
Wire Wire Line
	2550 2550 1900 2550
Wire Wire Line
	1900 2650 2550 2650
Wire Wire Line
	2550 2750 1900 2750
Wire Wire Line
	1900 2850 2550 2850
Wire Bus Line
	2700 3850 1900 3850
Wire Bus Line
	1900 1200 2700 1200
Entry Bus Bus
	1800 3750 1900 3850
Entry Bus Bus
	1800 1300 1900 1200
Entry Wire Line
	1800 5600 1900 5700
Entry Wire Line
	1800 5500 1900 5600
Entry Wire Line
	1800 5400 1900 5500
Entry Wire Line
	1800 5300 1900 5400
Entry Wire Line
	1800 5200 1900 5300
Entry Wire Line
	1800 5100 1900 5200
Entry Wire Line
	1800 5000 1900 5100
Entry Wire Line
	1800 4900 1900 5000
Wire Wire Line
	1900 5000 2550 5000
Wire Wire Line
	2550 5100 1900 5100
Wire Wire Line
	1900 5200 2550 5200
Wire Wire Line
	2550 5300 1900 5300
Wire Wire Line
	1900 5400 2550 5400
Wire Wire Line
	2550 5500 1900 5500
Wire Wire Line
	1900 5600 2550 5600
Wire Wire Line
	2550 5700 1900 5700
Text HLabel 950  900  0    60   Input ~ 0
D[0..15]
Wire Bus Line
	1800 900  950  900 
Text Label 1950 2150 0    60   ~ 0
D0
Text Label 1950 2250 0    60   ~ 0
D2
Text Label 1950 2350 0    60   ~ 0
D4
Text Label 1950 2450 0    60   ~ 0
D6
Text Label 1950 2550 0    60   ~ 0
D7
Text Label 1950 2650 0    60   ~ 0
D5
Text Label 1950 2750 0    60   ~ 0
D3
Text Label 1950 2850 0    60   ~ 0
D1
Text Label 1950 5000 0    60   ~ 0
D15
Text Label 1950 5100 0    60   ~ 0
D13
Text Label 1950 5200 0    60   ~ 0
D11
Text Label 1950 5300 0    60   ~ 0
D8
Text Label 1950 5400 0    60   ~ 0
D9
Text Label 1950 5500 0    60   ~ 0
D10
Text Label 1950 5600 0    60   ~ 0
D12
Text Label 1950 5700 0    60   ~ 0
D14
Text HLabel 950  1400 0    60   Input ~ 0
W
Text HLabel 950  1500 0    60   Input ~ 0
RAS
Text HLabel 950  1600 0    60   Input ~ 0
CASL
Text HLabel 950  1700 0    60   Input ~ 0
RA[0..8]
Wire Wire Line
	950  1400 2700 1400
Wire Wire Line
	950  1500 2700 1500
Wire Wire Line
	950  1600 2700 1600
Wire Bus Line
	950  1700 2700 1700
Wire Wire Line
	2700 4050 1700 4050
Wire Wire Line
	1700 4050 1700 1400
Connection ~ 1700 1400
Wire Wire Line
	2700 4150 1650 4150
Wire Wire Line
	1650 4150 1650 1500
Connection ~ 1650 1500
Wire Bus Line
	2700 4350 1300 4350
Wire Bus Line
	1300 4350 1300 1700
Text HLabel 950  4250 0    60   Input ~ 0
CASU
Wire Wire Line
	2700 4250 950  4250
$EndSCHEMATC
